#include "MehrbitRueckmeldung.h"



MehrbitRueckmeldung::MehrbitRueckmeldung(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "Mehrbitrueckmeldung"),
	m_SyncDecoder(timer, buffer),
	m_Databit(timer, buffer)
{
	SignalTimer::eTimer* p = new SignalTimer::eTimer[23 * 2];
	for (int i = 0; i < 23; ++i) {
		p[i * 2] = SignalTimer::m912;
		p[i * 2 + 1] = SignalTimer::m25;
	}
	m_SyncDecoder.SetPattern(p, 23 * 2);
	delete[] p;

	m_Databit.SetPattern(new SignalTimer::eTimer[4]{ SignalTimer::m456,SignalTimer::m25, SignalTimer::m456, SignalTimer::m25 }, 4);

}


MehrbitRueckmeldung::~MehrbitRueckmeldung()
{
}

void MehrbitRueckmeldung::Parse()
{
	if (m_State == idle) {
		m_iStateShift = m_SyncDecoder.FindNextAppearence();
		if (0 <= m_iStateShift && m_iStateShift <= 3) {
			m_State = busy;
			m_StartSample = m_Buffer->at(0).GetStart();
			m_Buffer->finishedReading(m_iStateShift + m_SyncDecoder.GetLength());
			m_Data = m_iStateShift;
		}
		else {
			m_State = failed;
		}
	}
	while (m_State == busy && m_Buffer->countReadable() >= m_Databit.GetLength()) {
		if (m_Databit.IsNextPattern()) {
			m_Data++;
			m_EndSample = m_Buffer->at(m_Databit.GetLength() - 1).GetEnd();
			m_Buffer->finishedReading(m_Databit.GetLength());
		}
		else {
			m_State = finished;
			break;
		}
	}
	
}

void MehrbitRueckmeldung::Clear() {
	ParseNode::Clear();
	m_iStateShift = 0;
}

void MehrbitRueckmeldung::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
	resultStrings[0] = "Mehrbitrueckmeldung";
	resultStrings[1] = resultStrings[2] = "Mehrbitrueckmeldung: " +std::to_string(data1) + " bits";
}
