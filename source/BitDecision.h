#pragma once

#include "ParseNode.h"
#include "FramePattern.h"
#include <map>

class BitDecision :
	public ParseNode
{
public:
	BitDecision(SignalTimer* timer, Ringspeicher* buffer, int bitLength);
	~BitDecision();

	void Parse();
	void Clear();
	std::list<ParseNode*>* GetNext();
	void SetNext(U64 bitMuster, ParseNode* next);

private:
	int m_bitLength;
	U64 m_readBits;
	std::map<U64, std::list<ParseNode*>> m_nextNodes;

};

