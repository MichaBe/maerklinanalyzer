#pragma once

#include "ParseNode.h"
#include "FramePattern.h"
#include "AnalyzerHelpers.h"
#include <string>

class CVLesen :
	public ParseNode
{
public:
	CVLesen(SignalTimer* timer, Ringspeicher* buffer);
	~CVLesen();

	void Parse();
	void Clear();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_pattern;
	const U32 m_MaskV = 0x3FF00;
	const U32 m_MaskI = 0xFC;
	const U8 m_MaskC = 0b11;
};

