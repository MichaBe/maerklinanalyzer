#include "Reserviert.h"



Reserviert::Reserviert(SignalTimer* timer, Ringspeicher* buffer, FramePattern* pattern, std::string description)
	: ParseNode(timer, buffer, "Reservierter Befehl")
{
	m_Pattern = pattern;
	m_Description = description;
}


Reserviert::~Reserviert()
{
}

void Reserviert::Parse()
{
	m_State = busy;
	if (m_Buffer->countReadable() >= m_Pattern->GetLength()) {
		if (m_Pattern->IsNextPattern()) {
			m_State = finished;
			m_StartSample = m_Buffer->at(0).GetStart();
			m_EndSample = m_Buffer->at(m_Pattern->GetLength() - 1).GetEnd();
			m_Buffer->finishedReading(m_Pattern->GetLength());
		}
	}
}

void Reserviert::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
	resultStrings[0] = resultStrings[1] = resultStrings[2] = "Reservierter Befehl: " + m_Description;
}
