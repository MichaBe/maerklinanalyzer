#include "MFXAnalyzer.h"
#include "MFXAnalyzerSettings.h"
#include <AnalyzerChannelData.h>

MFXAnalyzer::MFXAnalyzer()
:	Analyzer2(),  
	mSettings( new MFXAnalyzerSettings() ),
	mSimulationInitilized( false )
{
	SetAnalyzerSettings( mSettings.get() );
	m_SignalBuffer = new Ringspeicher(300);
}

MFXAnalyzer::~MFXAnalyzer()
{
	delete m_SignalBuffer;
	KillThread();
}

void MFXAnalyzer::SetupResults()
{
	mResults.reset( new MFXAnalyzerResults( this, mSettings.get() ) );
	SetAnalyzerResults( mResults.get() );
	mResults->AddChannelBubblesWillAppearOn( mSettings->m_ChannelGleissignal );
}

void MFXAnalyzer::WorkerThread()
{
	m_Channel = GetAnalyzerChannelData(mSettings->m_ChannelGleissignal);

	mSampleRateHz = GetSampleRate();
	timer = new SignalTimer(mSampleRateHz, m_Tolerance);
	
	m_Parser = new Parser(m_SignalBuffer, timer, m_Channel, &(*mResults));
	
	mResults->SetParser(m_Parser);

	m_Channel->AdvanceToNextEdge();
	U64 currentSample = m_Channel->GetSampleNumber();
	U64 currentParserSample;
	while (true) {
		currentParserSample = m_Parser->ParseSignal();
		if (currentParserSample != currentSample) {
			ReportProgress(currentParserSample);
			CheckIfThreadShouldExit();
			currentSample = currentParserSample;
		}
	}
}

bool MFXAnalyzer::NeedsRerun()
{
	return false;
}

U32 MFXAnalyzer::GenerateSimulationData( U64 minimum_sample_index, U32 device_sample_rate, SimulationChannelDescriptor** simulation_channels )
{
	if( mSimulationInitilized == false )
	{
		mSimulationDataGenerator.Initialize( GetSimulationSampleRate(), mSettings.get() );
		mSimulationInitilized = true;
	}

	return mSimulationDataGenerator.GenerateSimulationData( minimum_sample_index, device_sample_rate, simulation_channels );
}

U32 MFXAnalyzer::GetMinimumSampleRateHz()
{
	return 0;//TODO mSettings->mBitRate * 4;
}

const char* MFXAnalyzer::GetAnalyzerName() const
{
	return "Maerklin(R) MFX(R)";
}

const char* GetAnalyzerName()
{
	return "Maerklin(R) MFX(R)";
}

Analyzer* CreateAnalyzer()
{
	return new MFXAnalyzer();
}

void DestroyAnalyzer( Analyzer* analyzer )
{
	delete analyzer;
}