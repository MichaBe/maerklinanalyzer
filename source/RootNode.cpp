#include "RootNode.h"



RootNode::RootNode(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "Root"),
	m_DataFrameStart(timer, buffer),
	m_RueckmeldungStartSync(timer,buffer),
	m_RueckmeldungStartHalfSync(timer,buffer)
{
	SignalTimer::eTimer mS = SignalTimer::m50;
	SignalTimer::eTimer mL = SignalTimer::m100;
	m_RueckmeldungStartSync.SetPattern(new SignalTimer::eTimer[12]{ mL,mS,mL,mL,mS,mL,mL,mL,mS,mS,mS,mS }, 12);
	m_RueckmeldungStartHalfSync.SetPattern(new SignalTimer::eTimer[9]{ mL,mS,mL,mL,mL,mS,mS,mS,mS }, 9);
	m_DataFrameStart.SetPattern(new SignalTimer::eTimer[8]{ mL,mS,mL,mL,mS,mL,mS,mS }, 8);
	m_maxPatternLength = 12;
}


RootNode::~RootNode()
{
}

void RootNode::Parse()
{
	m_State = busy;
	int dfsn = m_DataFrameStart.FindNextAppearence();
	int rs = m_RueckmeldungStartSync.FindNextAppearence();
	int rhs = m_RueckmeldungStartHalfSync.FindNextAppearence();
	int nextPattern = -1;

	int rueckmeldung = -1;
	if (rs != -1 && rhs != -1) {
		rueckmeldung = (rs < rhs) ? rs : rhs; //Beide r�ckm. g�ltig -> minimum
	}
	else if (rs != -1 || rhs != -1)
		rueckmeldung = (rs < rhs) ? rhs : rs; //nur eins g�ltig -> maximum
	
	if (rueckmeldung != -1 && dfsn != -1) { //R�ckmeldung und dfsn g�ltig -> minimum finden
		if (rueckmeldung < dfsn) {
			m_IsRueckmeldung = true;
			m_State = finished_no_frame;
			nextPattern = rueckmeldung;
		}
		else {
			m_IsRueckmeldung = false;
			m_State = finished_no_frame;
			nextPattern = dfsn;
		}
	}
	else if (rueckmeldung != -1) { //Nur r�ckmeldung g�ltig
		m_IsRueckmeldung = true;
		m_State = finished_no_frame;
		nextPattern = rueckmeldung;
	}
	else if (dfsn != -1) { //Nur dfsn g�ltig
		m_IsRueckmeldung = false;
		m_State = finished_no_frame;
		nextPattern = dfsn;
	}
	else { //Kein Pattern gefunden -> Lesekopf ans ende verschieben
		m_Buffer->finishedReading(m_Buffer->countReadable() - m_maxPatternLength);
	}
	if (m_State == finished_no_frame) {
		//Verschieben bis nextPattern
		m_Buffer->finishedReading(nextPattern);
	}
}

std::list<ParseNode*>* RootNode::GetNext()
{
	std::list<ParseNode*>* returner = nullptr;

	if (m_IsRueckmeldung)
		returner = &m_RueckmeldungNextNode;
	else
		returner = &m_DataFrameNextNode;
	
	return returner;
}

void RootNode::SetNext(ParseNode * dataframeStartNode, ParseNode * rueckmeldungStartNode)
{
	m_DataFrameNextNode.clear();
	m_DataFrameNextNode.push_back(dataframeStartNode);
	m_RueckmeldungNextNode.clear();
	m_RueckmeldungNextNode.push_back(rueckmeldungStartNode);
}
