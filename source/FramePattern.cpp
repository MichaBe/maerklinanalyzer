#include "FramePattern.h"



FramePattern::FramePattern(SignalTimer* timer, Ringspeicher* buffer)
{
	m_Timer = timer;
	m_Buffer = buffer;
	m_Pattern = nullptr;
}


FramePattern::~FramePattern()
{
	delete[] m_Pattern;
}

void FramePattern::SetPattern(SignalTimer::eTimer* pattern, int length)
{
	if (m_Pattern != nullptr)
		delete[] m_Pattern;

	m_PatternStateCount = length;
	m_Pattern = new SignalTimer::eTimer[length];

	for (int i = 0; i < length; ++i) {
		m_Pattern[i] = pattern[i];
	}
}

void FramePattern::SetPattern(U32 pattern, int length)
{
	if (m_Pattern != nullptr)
		delete[] m_Pattern;

	U32 current = pattern;
	int neededStateCount = length;
	for (int i = 0; i < length; ++i) {
		neededStateCount = neededStateCount + (current % 2);
		current = current >> 1;
	}
	m_PatternStateCount = neededStateCount;
	m_Pattern = new SignalTimer::eTimer[m_PatternStateCount];

	current = pattern;
	int curIndex = m_PatternStateCount-1;
	for (int i = 0; i < length; ++i) {
		if (current & 1 == 1) {
			m_Pattern[curIndex] = SignalTimer::m50;
			curIndex--;
			m_Pattern[curIndex] = SignalTimer::m50;
			curIndex--;
		}
		else {
			m_Pattern[curIndex] = SignalTimer::m100;
			curIndex--;
		}
		current = current >> 1;
	}
}

bool FramePattern::IsNextPattern(int start)
{
	bool isPattern = true;
	SignalTimer::eTimer temp;
	bool isWellDefined;
	if (m_Buffer->countReadable() >= m_PatternStateCount) {
		for (int i = 0; i < m_PatternStateCount && isPattern; ++i) {
			isWellDefined = m_Buffer->at(start + i).GetRounded(temp);
			if (isWellDefined) {
				isPattern &= (temp == m_Pattern[i]);
				if (!isPattern) {
					m_debugString = "Failed at PatternState #" + std::to_string(i) + ", welldefined, but expected other state";
				}
			}
			else {
				m_debugString = "Failed at PatternState #" + std::to_string(i) + ", not welldefined, "+std::to_string(m_Buffer->at(start+i).GetDurationInSamples())+" samples, expected "+std::to_string(m_Timer->at(m_Pattern[i]))+" samples";
				isPattern = false;
			}
		}
	}
	else {
		m_debugString = "Failed: not enough readable";
		isPattern = false;
	}
	return isPattern;
}

int FramePattern::FindNextAppearence(int start)
{
	int firstAppearence = -1;
	for (int i = start; i <= m_Buffer->countReadable()-m_PatternStateCount; ++i) {
		if (IsNextPattern(i)) {
			firstAppearence = i;
			break;
		}
	}
	return firstAppearence;
}

int FramePattern::GetLength()
{
	return m_PatternStateCount;
}

std::string FramePattern::GetLastDebugInfo()
{
	return m_debugString;
}
