#pragma once

#include "ParseNode.h"
#include "FramePattern.h"

class Reserviert :
	public ParseNode
{
public:
	Reserviert(SignalTimer* timer, Ringspeicher* buffer, FramePattern* pattern, std::string description);
	~Reserviert();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern* m_Pattern;
	std::string m_Description;

};

