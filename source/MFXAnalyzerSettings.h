#ifndef MFX_ANALYZER_SETTINGS
#define MFX_ANALYZER_SETTINGS

#include <AnalyzerSettings.h>
#include <AnalyzerTypes.h>

class MFXAnalyzerSettings : public AnalyzerSettings
{
public:
	MFXAnalyzerSettings();
	virtual ~MFXAnalyzerSettings();

	virtual bool SetSettingsFromInterfaces();
	void UpdateInterfacesFromSettings();
	virtual void LoadSettings( const char* settings );
	virtual const char* SaveSettings();

	
	Channel m_ChannelGleissignal;
	enum seperator { semicolon, comma } m_Seperator = semicolon;

protected:
	std::auto_ptr<AnalyzerSettingInterfaceChannel> mInputChannelInterface;
	std::auto_ptr<AnalyzerSettingInterfaceNumberList> m_InputSemicolonIsSeperator;
};

#endif //MFX_ANALYZER_SETTINGS
