#include "FrameEnde.h"



FrameEnde::FrameEnde(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "Frame-Ende"),
	m_Sync(timer, buffer)
{
	Clear();
	SignalTimer::eTimer mL = SignalTimer::m100;
	SignalTimer::eTimer mS = SignalTimer::m50;
	m_Sync.SetPattern(new SignalTimer::eTimer[6]{ mL,mS,mL,mL,mS,mL }, 6);
}


FrameEnde::~FrameEnde()
{
}

void FrameEnde::Parse()
{
	m_State = busy;
	if (m_Buffer->countReadable() >= 22) {
		//Schauen, ob 8 bits lesbar sind
		U64 bits;
		int stateCount;
		bool bSuccess = m_Buffer->readBitsBigEndian(&bits, 8, &stateCount);
		if (bSuccess) {
			//Schauen, ob nach den 8 bits ein sync-pattern kommt
			bSuccess = m_Sync.IsNextPattern(stateCount);
		}
		if (bSuccess) {
			m_State = finished_no_frame;
			m_IsChecksumme = true;
		}
		else {
			m_State = finished_no_frame;
			m_IsChecksumme = false;
		}
	}
}

void FrameEnde::Clear()
{
	ParseNode::Clear();
	m_IsChecksumme = false;
}

std::list<ParseNode*>* FrameEnde::GetNext()
{
	std::list<ParseNode*>* returner = nullptr;
	
	if (m_IsChecksumme)
		returner = &m_NextChecksumme;
	else
		returner = &m_NextAndere;
	
	return returner;
}

void FrameEnde::SetNext(ParseNode * ChecksummeNode, std::list<ParseNode*>* andereNodes)
{
	m_NextChecksumme.clear();
	m_NextAndere.clear();
	m_NextChecksumme.push_back(ChecksummeNode);

	for (auto curN : *andereNodes)
		m_NextAndere.push_back(curN);
}
