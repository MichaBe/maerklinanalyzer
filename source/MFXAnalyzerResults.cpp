#include "MFXAnalyzerResults.h"
#include <AnalyzerHelpers.h>
#include "MFXAnalyzer.h"
#include "MFXAnalyzerSettings.h"
#include "Parser.h"
#include <iostream>
#include <fstream>

MFXAnalyzerResults::MFXAnalyzerResults( MFXAnalyzer* analyzer, MFXAnalyzerSettings* settings )
:	AnalyzerResults(),
	mSettings( settings ),
	mAnalyzer( analyzer )
{

}

MFXAnalyzerResults::~MFXAnalyzerResults()
{
}

void MFXAnalyzerResults::GenerateBubbleText( U64 frame_index, Channel& channel, DisplayBase display_base )
{
	ClearResultStrings();

	Frame frame = GetFrame( frame_index );

	std::array<std::string, 3> result;
	m_Parser->GetResultString(&frame, &display_base, result);
	for(int i = 0; i < 3; ++i)
		AddResultString(result[i].c_str());
	
}

void MFXAnalyzerResults::GenerateExportFile( const char* file, DisplayBase display_base, U32 export_type_user_id )
{
	std::ofstream file_stream( file, std::ios::out );
	char seperator = mSettings->m_Seperator == MFXAnalyzerSettings::semicolon ? ';' : ',';

	U64 trigger_sample = mAnalyzer->GetTriggerSample();
	U32 sample_rate = mAnalyzer->GetSampleRate();

	file_stream << "Zeitpunkt (ms)" << seperator << "Frame-ID" << seperator << "Framekennung" << seperator << "Beschreibung";
	

	U64 num_frames = GetNumFrames();
	for( U32 i=0; i < num_frames; i++ )
	{
		Frame frame = GetFrame( i );
		
		file_stream << m_Parser->GetExportString(&frame, &display_base, trigger_sample, seperator);

		if( UpdateExportProgressAndCheckForCancel( i, num_frames ) == true )
		{
			file_stream.close();
			return;
		}
	}

	file_stream.close();
}

void MFXAnalyzerResults::GenerateFrameTabularText( U64 frame_index, DisplayBase display_base )
{
#ifdef SUPPORTS_PROTOCOL_SEARCH
	Frame frame = GetFrame( frame_index );
	ClearTabularText();

	std::array<std::string, 3>  result;
	m_Parser->GetResultString(&frame, &display_base,result);
	AddTabularText(result[0].c_str());
#endif
}

void MFXAnalyzerResults::GeneratePacketTabularText( U64 packet_id, DisplayBase display_base )
{
	//not supported

}

void MFXAnalyzerResults::GenerateTransactionTabularText( U64 transaction_id, DisplayBase display_base )
{
	//not supported
}

void MFXAnalyzerResults::SetParser(Parser * parser)
{
	m_Parser = parser;
}
