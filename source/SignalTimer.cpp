#include "SignalTimer.h"



SignalTimer::SignalTimer(U32 sampleRate, float tolerancePercentage)
{
	m_SampleCount = new U64[m_TimerLength];
	m_SampleCountUpperBound = new U64[m_TimerLength];
	m_SampleCountLowerBound = new U64[m_TimerLength];
	SetSamplerateAndTolerance(sampleRate, tolerancePercentage);
}


SignalTimer::~SignalTimer()
{
	delete[] m_SampleCount;
	delete[] m_SampleCountUpperBound;
	delete[] m_SampleCountLowerBound;
}

void SignalTimer::SetSamplerateAndTolerance(U32 sampleRate, float tolerance)
{
	m_SampleRate = sampleRate;
	m_SampleCount[m25] =  (m_SampleRate / 1000000) * 25;
	m_SampleCount[m50] =  (m_SampleRate / 1000000) * 50;
	m_SampleCount[m100] = (m_SampleRate / 1000000) * 100;
	m_SampleCount[m456] = (m_SampleRate / 1000000) * (456 - 25);
	m_SampleCount[m912] = (m_SampleRate / 1000000) * (912 - 25);
	m_SampleCount[m6400] = (m_SampleRate / 1000000) * 6400;

	m_Tolerance = tolerance;
	for (int i = 0; i < m_TimerLength; ++i) {
		m_SampleCountUpperBound[i] = m_SampleCount[i] * (1.0f + m_Tolerance*0.01f);
		m_SampleCountLowerBound[i] = m_SampleCount[i] * (1.0f - m_Tolerance*0.01f);
	}
	m_SampleCountLowerBound[m25] = (m_SampleRate / 1000000) * 20;
	m_SampleCountUpperBound[m25] = (m_SampleRate / 1000000) * 30;
}

bool SignalTimer::GetRoundedSignalType(U64 sampleCount, eTimer& outTimer)
{
	bool bfoundSignal = false;
	for (int i = 0; i < m_TimerLength && !bfoundSignal; ++i) {
		if (m_SampleCountLowerBound[i] <= sampleCount && sampleCount <= m_SampleCountUpperBound[i]) {
			outTimer = (eTimer)i;
			bfoundSignal = true;
		}
	}
	return bfoundSignal;
}

U64 SignalTimer::at(eTimer n)
{
	if (0 <= n && n < m_TimerLength)
		return m_SampleCount[n];
	else
		return 0;
}

U32 SignalTimer::getSamplerate()
{
	return m_SampleRate;
}
