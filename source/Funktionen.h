#pragma once

#include <AnalyzerHelpers.h>
#include "ParseNode.h"
#include "FramePattern.h"
#include <string>

class Funktionen :
	public ParseNode
{
public:
	Funktionen(SignalTimer* timer, Ringspeicher* buffer);
	~Funktionen();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_Fkurz;
	FramePattern m_Ferweitert0;
	FramePattern m_Ferweitert1;
	FramePattern m_Feinzel;
};

