#include "MFXAnalyzerSettings.h"
#include <AnalyzerHelpers.h>


MFXAnalyzerSettings::MFXAnalyzerSettings()
:	m_ChannelGleissignal( UNDEFINED_CHANNEL )
{
	mInputChannelInterface.reset( new AnalyzerSettingInterfaceChannel() );
	mInputChannelInterface->SetTitleAndTooltip( "MFX Gleissignal (Digital)", "Gleissignal");
	mInputChannelInterface->SetChannel( m_ChannelGleissignal );
	
	m_InputSemicolonIsSeperator.reset(new AnalyzerSettingInterfaceNumberList());
	m_InputSemicolonIsSeperator->AddNumber(semicolon, "Semikolon", "Semikolon");
	m_InputSemicolonIsSeperator->AddNumber(comma, "Komma", "Komma");
	m_InputSemicolonIsSeperator->SetTitleAndTooltip("Seperator fuer Export", "");
	m_InputSemicolonIsSeperator->SetNumber(m_Seperator);

	AddInterface(mInputChannelInterface.get());
	AddInterface(m_InputSemicolonIsSeperator.get());

	AddExportOption( 0, "Export as text/csv file" );
	AddExportExtension( 0, "text", "txt" );
	AddExportExtension( 0, "csv", "csv" );

	ClearChannels();
	AddChannel( m_ChannelGleissignal, "Gleissignal", false );
}

MFXAnalyzerSettings::~MFXAnalyzerSettings()
{
}

bool MFXAnalyzerSettings::SetSettingsFromInterfaces()
{
	m_ChannelGleissignal = mInputChannelInterface->GetChannel();
	m_Seperator = (seperator)(int)m_InputSemicolonIsSeperator->GetNumber();
	ClearChannels();
	AddChannel(m_ChannelGleissignal, "Gleissignal", true);
	return true;
}

void MFXAnalyzerSettings::UpdateInterfacesFromSettings()
{
	mInputChannelInterface->SetChannel(m_ChannelGleissignal);
	m_InputSemicolonIsSeperator->SetNumber(m_Seperator);
}

void MFXAnalyzerSettings::LoadSettings( const char* settings )
{
	SimpleArchive text_archive;
	text_archive.SetString( settings );
	double tempSeperator;
	
	text_archive >> m_ChannelGleissignal;
	text_archive >> tempSeperator;
	m_Seperator = (seperator)(int)tempSeperator;

	ClearChannels();
	AddChannel(m_ChannelGleissignal, "Gleissignal", true);

	UpdateInterfacesFromSettings();
}

const char* MFXAnalyzerSettings::SaveSettings()
{
	SimpleArchive text_archive;

	text_archive << m_ChannelGleissignal;
	text_archive << (double)m_Seperator;

	return SetReturnString( text_archive.GetString() );
}
