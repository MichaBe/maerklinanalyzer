#pragma once

#include <array>
#include "AnalyzerTypes.h"
#include "SignalState.h"

class Ringspeicher
{
public:
	Ringspeicher(int length);
	~Ringspeicher();

	const SignalState& at(int i);

	int countReadable();
	int countWriteable();

	SignalState& write();

	bool finishedReading(int n);
	bool readBitsBigEndian(U64* outBits, int length, int* outStateCount, int startState = 0);

private:
	int m_Length;
	SignalState* m_Buffer;
	int m_CurrentReadZero;
	int m_CurrentWriteZero;
};

