#pragma once

#include "ParseNode.h"
#include "FramePattern.h"
#include "AnalyzerHelpers.h"

class AdressenZuweisung :
	public ParseNode
{
public:
	AdressenZuweisung(SignalTimer* timer, Ringspeicher* buffer);
	~AdressenZuweisung();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_StartPattern;
	const U64 m_aMask = 0x3FFF00000000;
	const U64 m_uMask = 0xFFFFFFFF;
};

