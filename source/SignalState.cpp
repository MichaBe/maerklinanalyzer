#include "SignalState.h"

SignalState::SignalState()
{
	m_StartSample = m_EndSample = 0;
	m_RoundedIsWellDefined = false;
}

SignalState::SignalState(U64 start, U64 end, SignalTimer::eTimer rounded)
{
	m_StartSample = start;
	m_EndSample = end;
	m_rounded = rounded;
	m_RoundedIsWellDefined = true;
}

SignalState::~SignalState()
{
}

void SignalState::Set(U64 start, U64 end) 
{
	m_StartSample = start;
	m_EndSample = end;
	m_RoundedIsWellDefined = false;
}
void SignalState::Set(U64 start, U64 end, SignalTimer::eTimer rounded)
{
	m_StartSample = start;
	m_EndSample = end;
	m_rounded = rounded;
	m_RoundedIsWellDefined = true;
}

U64 SignalState::GetDurationInSamples() const
{
	return m_EndSample - m_StartSample;
}

U64 SignalState::GetStart() const
{
	return m_StartSample;
}

U64 SignalState::GetEnd() const
{
	return m_EndSample;
}

bool SignalState::GetRounded(SignalTimer::eTimer& out) const
{
	if (m_RoundedIsWellDefined)
		out = m_rounded;
	return m_RoundedIsWellDefined;
}
