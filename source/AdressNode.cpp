#include "AdressNode.h"

AdressNode::AdressNode(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "Adresse"),
	m_AdressStart7Bit(timer, buffer),
	m_AdressStart9Bit(timer, buffer),
	m_AdressStart11Bit(timer, buffer),
	m_AdressStart14Bit(timer, buffer)
{
	m_AdressStart7Bit.SetPattern(0b10, 2);
	m_AdressStart9Bit.SetPattern(0b110, 3);
	m_AdressStart11Bit.SetPattern(0b1110, 4);
	m_AdressStart14Bit.SetPattern(0b1111, 4);

	Clear();
}

AdressNode::~AdressNode()
{
}

void AdressNode::Parse()
{
	m_State = busy;
	m_StartSample = m_Buffer->at(0).GetStart();
	if (m_Buffer->countReadable() >= 8 && m_Data == 0) {
		if (m_AdressStart7Bit.IsNextPattern()) {
			m_Type = 7;
			m_Buffer->finishedReading(m_AdressStart7Bit.GetLength());
		}
		else if (m_AdressStart9Bit.IsNextPattern()) {
			m_Type = 9;
			m_Buffer->finishedReading(m_AdressStart9Bit.GetLength());
		}
		else if (m_AdressStart11Bit.IsNextPattern()) {
			m_Type = 11;
			m_Buffer->finishedReading(m_AdressStart11Bit.GetLength());
		}
		else if (m_AdressStart14Bit.IsNextPattern()) {
			m_Type = 14;
			m_Buffer->finishedReading(m_AdressStart14Bit.GetLength());
		}
		else {
			m_State = failed;
		}
	}
	if (m_State != failed && m_Buffer->countReadable() >= m_Type * 2) {
		int iStateshift = 0;
		bool bSuccess = m_Buffer->readBitsBigEndian(&m_Data, m_Type, &iStateshift);
		
		if (bSuccess) {
			m_State = finished;
			m_EndSample = m_Buffer->at(iStateshift - 1).GetEnd();
			m_Buffer->finishedReading(iStateshift);
		}
		else
			m_State = failed;
	}
}


void AdressNode::GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings)
{
	char numberString[64];
	AnalyzerHelpers::GetNumberString(data1, *display, type, numberString, 64);
	
	resultStrings[0] = "Adresse";
	resultStrings[1] = "Adresse (" + std::to_string(type) + " bits)";
	resultStrings[2] = resultStrings[1] + ": " + numberString;

	if (data1 == 0) {
		resultStrings[1] = resultStrings[1] + " (Broadcast)";
		resultStrings[2] = resultStrings[2] + " (Broadcast)";
	}
}
