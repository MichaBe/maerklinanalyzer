#ifndef MFX_ANALYZER_H
#define MFX_ANALYZER_H

#include <Analyzer.h>
#include "MFXAnalyzerResults.h"
#include "MFXSimulationDataGenerator.h"

#include "Ringspeicher.h"
#include "SignalState.h"
#include "FramePattern.h"
#include "SignalTimer.h"
#include "Parser.h"

#include <iostream>
#include <fstream>

class MFXAnalyzerSettings;
class ANALYZER_EXPORT MFXAnalyzer : public Analyzer2
{
public:
	MFXAnalyzer();
	virtual ~MFXAnalyzer();

	virtual void SetupResults();
	virtual void WorkerThread();

	virtual U32 GenerateSimulationData( U64 newest_sample_requested, U32 sample_rate, SimulationChannelDescriptor** simulation_channels );
	virtual U32 GetMinimumSampleRateHz();

	virtual const char* GetAnalyzerName() const;
	virtual bool NeedsRerun();
	U32 mSampleRateHz;


protected: //vars
	SignalTimer* timer;
	std::auto_ptr< MFXAnalyzerSettings > mSettings;
	std::auto_ptr< MFXAnalyzerResults > mResults;
	AnalyzerChannelData* m_Channel;

	MFXSimulationDataGenerator mSimulationDataGenerator;
	bool mSimulationInitilized;

	Ringspeicher* m_SignalBuffer;
	Parser* m_Parser;
	const int m_Tolerance = 10;

};

extern "C" ANALYZER_EXPORT const char* __cdecl GetAnalyzerName();
extern "C" ANALYZER_EXPORT Analyzer* __cdecl CreateAnalyzer( );
extern "C" ANALYZER_EXPORT void __cdecl DestroyAnalyzer( Analyzer* analyzer );

#endif //MFX_ANALYZER_H
