#ifndef MFX_ANALYZER_RESULTS
#define MFX_ANALYZER_RESULTS

#include <AnalyzerResults.h>

class MFXAnalyzer;
class MFXAnalyzerSettings;
class Parser;

class MFXAnalyzerResults : public AnalyzerResults
{
public:
	MFXAnalyzerResults( MFXAnalyzer* analyzer, MFXAnalyzerSettings* settings );
	virtual ~MFXAnalyzerResults();

	virtual void GenerateBubbleText( U64 frame_index, Channel& channel, DisplayBase display_base );
	virtual void GenerateExportFile( const char* file, DisplayBase display_base, U32 export_type_user_id );

	virtual void GenerateFrameTabularText(U64 frame_index, DisplayBase display_base );
	virtual void GeneratePacketTabularText( U64 packet_id, DisplayBase display_base );
	virtual void GenerateTransactionTabularText( U64 transaction_id, DisplayBase display_base );

	void SetParser(Parser* parser);

protected: 
	MFXAnalyzerSettings* mSettings;
	MFXAnalyzer* mAnalyzer;
	Parser* m_Parser;
};

#endif //MFX_ANALYZER_RESULTS
