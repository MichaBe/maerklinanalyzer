#pragma once

#include "ParseNode.h"
#include "FramePattern.h"
#include "AnalyzerHelpers.h"

class Zentrale :
	public ParseNode
{
public:
	Zentrale(SignalTimer* timer, Ringspeicher* buffer);
	~Zentrale();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);
	
private:
	FramePattern m_StartPattern;
	const U32 m_zMask = 0x0FFFF;
	const U64 m_uidMask = 0x0FFFFFFFF0000;
};

