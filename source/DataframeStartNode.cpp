#include "DataframeStartNode.h"



DataframeStartNode::DataframeStartNode(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "Framestart"),
	m_Sync(timer,buffer)
{
	m_Sync.SetPattern(new SignalTimer::eTimer[6]{ SignalTimer::m100, SignalTimer::m50, SignalTimer::m100, SignalTimer::m100, SignalTimer::m50, SignalTimer::m100 }, 6);
	Clear();
}


DataframeStartNode::~DataframeStartNode()
{
}

void DataframeStartNode::Parse()
{
	m_State = busy;
	if (m_Sync.IsNextPattern()) {
		SignalTimer::eTimer t1, t2;
		if (m_Buffer->at(m_Sync.GetLength()).GetRounded(t1)
			&& m_Buffer->at(m_Sync.GetLength()+1).GetRounded(t2)) {
			if (t1 == SignalTimer::eTimer::m50 && t1 == t2) {
				m_State = finished;
				m_StartSample = m_Buffer->at(0).GetStart();
				m_EndSample = m_Buffer->at(m_Sync.GetLength() - 1).GetEnd();
				m_Buffer->finishedReading(m_Sync.GetLength());
			}
		}
	}
	if (m_State != finished) {
		m_State = failed;
	}
}

void DataframeStartNode::GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings)
{
	for(int i = 0; i < 3; ++i)
		resultStrings[i]= "Framestart";
}
