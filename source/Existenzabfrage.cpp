#include "Existenzabfrage.h"



Existenzabfrage::Existenzabfrage(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "Existenzabfrage"),
	m_Startpattern(timer,buffer)
{
	m_Startpattern.SetPattern(0b100, 3);
}


Existenzabfrage::~Existenzabfrage()
{
}

void Existenzabfrage::Parse()
{
	m_State = busy;
	m_StartSample = m_Buffer->at(0).GetStart();
	if (m_Buffer->countReadable() >= m_Startpattern.GetLength()+64) {
		int iStateOffset = 0;
		if (m_Startpattern.IsNextPattern()) {
			iStateOffset = m_Startpattern.GetLength();
			int iReadStateLength = 0;
			bool bSuccess = m_Buffer->readBitsBigEndian(&m_Data, 32, &iReadStateLength, iStateOffset);
			iStateOffset += iReadStateLength;
			if (bSuccess) {
				m_EndSample = m_Buffer->at(iStateOffset - 1).GetEnd();
				m_Buffer->finishedReading(iStateOffset);
				m_State = finished;
			}
			else
				m_State = failed;
		}
		else
			m_State = failed;
	}
}

void Existenzabfrage::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
	char uidNumberString[64];
	
	AnalyzerHelpers::GetNumberString(data1, *display, 32, uidNumberString, 64);

	resultStrings[0] = "Existenzabfrage";
	resultStrings[1] = (std::string)"Existenzabfrage: UID=" + uidNumberString;
	resultStrings[2] = resultStrings[1];
}
