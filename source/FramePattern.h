#pragma once

#include "AnalyzerTypes.h"
#include "Ringspeicher.h"
#include "SignalState.h"
#include "SignalTimer.h"
#include <string>

class FramePattern
{
public:
	FramePattern(SignalTimer* timer, Ringspeicher* buffer);
	~FramePattern();

	void SetPattern(SignalTimer::eTimer* pattern, int length);
	void SetPattern(U32 pattern, int length);

	bool IsNextPattern(int start = 0);
	int FindNextAppearence(int start = 0);
	int GetLength();
	std::string GetLastDebugInfo();

private:
	SignalTimer* m_Timer;
	Ringspeicher* m_Buffer;
	SignalTimer::eTimer* m_Pattern;
	int m_PatternStateCount;
	std::string m_debugString;
};

