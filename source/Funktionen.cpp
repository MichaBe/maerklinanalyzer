#include "Funktionen.h"



Funktionen::Funktionen(SignalTimer* timer, Ringspeicher* buffer)
	:ParseNode(timer, buffer, "Funktionen"),
	m_Fkurz(timer, buffer),
	m_Ferweitert0(timer,buffer),
	m_Ferweitert1(timer,buffer),
	m_Feinzel(timer,buffer)
{
	m_Fkurz.SetPattern(0b010, 3);
	m_Ferweitert0.SetPattern(0b0110, 4);
	m_Ferweitert1.SetPattern(0b0111, 4);
	m_Feinzel.SetPattern(0b100, 3);
	Clear();
}

Funktionen::~Funktionen()
{
}

void Funktionen::Parse()
{
	m_State = busy;
	if (m_Buffer->countReadable() >= 37) {
		int stateShift = 0;
		m_StartSample = m_Buffer->at(0).GetStart();
		bool bSuccess = false;
		if (m_Fkurz.IsNextPattern()) {
			m_Type = 1;
			bSuccess = m_Buffer->readBitsBigEndian(&m_Data, 7, &stateShift);
		}
		else if (m_Ferweitert0.IsNextPattern()) {
			m_Type = 2;
			bSuccess = m_Buffer->readBitsBigEndian(&m_Data, 12, &stateShift);
		}
		else if (m_Ferweitert1.IsNextPattern()) {
			m_Type = 3;
			bSuccess = m_Buffer->readBitsBigEndian(&m_Data, 20, &stateShift);
		}
		else if (m_Feinzel.IsNextPattern()) {
			m_Type = 4;
			bSuccess = m_Buffer->readBitsBigEndian(&m_Data, 12, &stateShift);
		}
		if (bSuccess) {
			m_EndSample = m_Buffer->at(stateShift - 1).GetEnd();
			m_Buffer->finishedReading(stateShift);
		}

		m_State = bSuccess ? finished : failed;
	}
}

void Funktionen::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string,3>& resultStrings)
{
	char FunktionString[64];
	resultStrings[0] = "Funktionen";
	if (type == 1) {
		resultStrings[1] = "Funktionen (kurz)";
		resultStrings[2] = "Funktionen (kurz): F3-F0=";
		AnalyzerHelpers::GetNumberString(data1, *display, 4,FunktionString, 64);
		resultStrings[2] = resultStrings[2] + FunktionString + ", F4-F15 aus";
	}
	else if (type == 2) {
		resultStrings[1] = "Funktionen (erweitert)";
		resultStrings[2] = "Funktionen (erweitert): F7-F0=";
		AnalyzerHelpers::GetNumberString(data1, *display, 8, FunktionString, 64);
		resultStrings[2] = resultStrings[2] + FunktionString + ", F8-F15 aus";
	}
	else if (type == 3) {
		resultStrings[1] = "Funktionen (erweitert, lang)";
		resultStrings[2] = "Funktionen (erweitert, lang): F15-F0=";
		AnalyzerHelpers::GetNumberString(data1, *display, 16, FunktionString, 64);
		resultStrings[2] = resultStrings[2] + FunktionString;
	}
	else if (type == 4) {
		resultStrings[1] = "Funktionen (einzel)";
		resultStrings[2] = "Funktionen (einzel): ";
		U64 dFunktion = (data1 & 0b111111100) >> 2;
		AnalyzerHelpers::GetNumberString(dFunktion, DisplayBase::Decimal, 7, FunktionString, 64);
		resultStrings[2] = resultStrings[2] + FunktionString + "=" + (data1 & 0b1 ? "ein" : "aus");
	}
}
