#pragma once

#include "ParseNode.h"
#include "FramePattern.h"
#include <AnalyzerChannelData.h>

class MehrbitRueckmeldung :
	public ParseNode
{
public:
	MehrbitRueckmeldung(SignalTimer* timer, Ringspeicher* buffer);
	~MehrbitRueckmeldung();

	void Parse();
	void Clear();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_SyncDecoder;
	FramePattern m_Databit;
	int m_iStateShift;
};

