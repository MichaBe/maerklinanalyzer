#pragma once

#include <LogicPublicTypes.h>
#include <AnalyzerResults.h>
#include <list>
#include <array>
#include "SignalTimer.h"
#include "SignalState.h"
#include "Ringspeicher.h"

class ParseNode
{
public:
	enum state { idle, busy, finished, finished_no_frame, failed };
	ParseNode(SignalTimer* timer, Ringspeicher* buffer, std::string name);
	~ParseNode();

	virtual void Parse() = 0;
	bool GenerateFrame(Frame* frame);
	virtual void Clear();
	virtual void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string,3>& resultStrings);
	virtual std::list<ParseNode*>* GetNext();
	void AddNext(ParseNode* next);
	state GetState();
	int GetID();
	std::string GetName();

private:
	static int m_ID;
	int m_NodeID;
	std::list<ParseNode*> m_Next;
	std::string m_NodeName;

protected:
	SignalTimer* m_Timer;
	Ringspeicher* m_Buffer;
	state m_State;
	U64 m_StartSample;
	U64 m_EndSample;
	U8 m_Type;
	U64 m_Data;
};