#pragma once

#include "AnalyzerHelpers.h"
#include "ParseNode.h"
#include "FramePattern.h"
#include <string>

class KonfigurationStart :
	public ParseNode
{
public:
	KonfigurationStart(SignalTimer* timer, Ringspeicher* buffer);
	~KonfigurationStart();

	void Parse();
	void Clear();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_pattern;
};

