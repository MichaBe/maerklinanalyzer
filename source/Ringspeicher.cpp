#include "Ringspeicher.h"

Ringspeicher::Ringspeicher(int length)
{
	m_Length = length;
	m_Buffer = new SignalState[length];
	m_CurrentReadZero = 0;
	m_CurrentWriteZero = 0;
}
Ringspeicher::~Ringspeicher()
{
	delete[] m_Buffer;
}

const SignalState& Ringspeicher::at(int i)
{
	return m_Buffer[(i + m_CurrentReadZero) % m_Length];
}

int Ringspeicher::countReadable()
{
	if (m_CurrentReadZero <= m_CurrentWriteZero) {
		return m_CurrentWriteZero - m_CurrentReadZero;
	}
	else
		return m_Length - m_CurrentReadZero + m_CurrentWriteZero;
}
int Ringspeicher::countWriteable()
{
	return m_Length - countReadable() - 1;
}

SignalState& Ringspeicher::write()
{
	m_CurrentWriteZero = (m_CurrentWriteZero + 1) % m_Length;
	return m_Buffer[(m_CurrentWriteZero - 1 + m_Length) % m_Length];
}

bool Ringspeicher::finishedReading(int n)
{
	if (countReadable() >= n) {
		m_CurrentReadZero = (m_CurrentReadZero + n) % m_Length;
		return true;
	}
	else
		return false;
}

bool Ringspeicher::readBitsBigEndian(U64 * outBits, int length, int* outStateCount, int startState)
{
	bool bReturner = (length < 64);
	U64 bits = 0b0;
	int iCurRead = startState;
	if (bReturner && length < countReadable() + startState) {
		SignalTimer::eTimer t0, t1;
		bool b0, b1;
		for (int i = 0; i < length && bReturner; ++i) {
			b0 = at(iCurRead).GetRounded(t0);
			b1 = at(iCurRead + 1).GetRounded(t1);
			if (b0 && b1) {
				if (t0 == t1 && t0 == SignalTimer::m50) {
					//1
					bits = bits << 1;
					bits += 1;
					iCurRead += 2;
				}
				else if (t0 == SignalTimer::m100) {
					//0
					bits = bits << 1;
					iCurRead += 1;
				}
			}
			else if (b0) {
				if (t0 == SignalTimer::m100) {
					//0
					bits = bits << 1;
					iCurRead += 1;
				}
				else
					bReturner = false;
			}
			else
				bReturner = false;
		}
	}
	if (bReturner) {
		*outStateCount = iCurRead-startState;
		*outBits = bits;
	}

	return bReturner;
}
