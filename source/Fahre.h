#pragma once

#include <AnalyzerHelpers.h>

#include "ParseNode.h"
#include "SignalTimer.h"
#include "Ringspeicher.h"
#include "FramePattern.h"
#include <string>

class Fahre :
	public ParseNode
{
public:
	Fahre(SignalTimer* timer, Ringspeicher* buffer);
	~Fahre();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_Kurz;
	FramePattern m_Lang;
};

