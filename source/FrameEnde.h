#pragma once

#include "ParseNode.h"
#include "FramePattern.h"

class FrameEnde :
	public ParseNode
{
public:
	FrameEnde(SignalTimer* timer, Ringspeicher* buffer);
	~FrameEnde();

	void Parse();
	void Clear();
	std::list<ParseNode*>* GetNext();
	void SetNext(ParseNode* ChecksummenNode, std::list<ParseNode*>* andereNodes);

private:
	std::list<ParseNode*> m_NextChecksumme;
	std::list<ParseNode*> m_NextAndere;
	bool m_IsChecksumme;
	FramePattern m_Sync;
};

