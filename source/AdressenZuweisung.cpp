#include "AdressenZuweisung.h"



AdressenZuweisung::AdressenZuweisung(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "Adressenzuweisung"),
	m_StartPattern(timer, buffer)
{
	m_StartPattern.SetPattern(0b011, 3);
}


AdressenZuweisung::~AdressenZuweisung()
{
}

void AdressenZuweisung::Parse()
{
	m_State = busy;
	m_StartSample = m_Buffer->at(0).GetStart();
	if (m_Buffer->countReadable() >= 97) {
		if (m_StartPattern.IsNextPattern()) {
			int iStateShift = 0;
			bool bSuccess = m_Buffer->readBitsBigEndian(&m_Data, 46, &iStateShift, m_StartPattern.GetLength());
			if (bSuccess) {
				iStateShift += m_StartPattern.GetLength();
				m_EndSample = m_Buffer->at(iStateShift - 1).GetEnd();
				m_Buffer->finishedReading(iStateShift);
				m_State = finished;
			}
			else
				m_State = failed;
		}
		else
			m_State = failed;
	}
}

void AdressenZuweisung::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
	char UID[64];
	char A[64];
	
	AnalyzerHelpers::GetNumberString(data1&m_uMask, *display, 32, UID, 64);
	AnalyzerHelpers::GetNumberString((data1&m_aMask) >> 32, *display, 14, A, 64);

	resultStrings[0] = "Adressenzuweisung";
	resultStrings[1] = resultStrings[0] + ": " + UID + "<-" + A;
	resultStrings[2] = resultStrings[0] + ": UID " + UID + "<- Schienenadresse " + A;
}
