#pragma once

#include "ParseNode.h"
#include "FramePattern.h"

class RootNode :
	public ParseNode
{
public:
	RootNode(SignalTimer* timer, Ringspeicher* buffer);
	~RootNode();

	void Parse();
	std::list<ParseNode*>* GetNext();
	void SetNext(ParseNode* dataframeStartNode, ParseNode* rueckmeldungStartNode);

private:
	FramePattern m_DataFrameStart;
	FramePattern m_RueckmeldungStartSync;
	FramePattern m_RueckmeldungStartHalfSync;
	std::list<ParseNode*> m_DataFrameNextNode;
	std::list<ParseNode*> m_RueckmeldungNextNode;
	bool m_IsRueckmeldung;
	int m_maxPatternLength;
	
};

