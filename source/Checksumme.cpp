#include "Checksumme.h"



Checksumme::Checksumme(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "Checksumme")
{
	Clear();
}


Checksumme::~Checksumme()
{
}

void Checksumme::Parse()
{
	m_State = busy;
	if (m_Buffer->countReadable() >= 16) {
		int iStateShift = 0;
		bool bSuccess = m_Buffer->readBitsBigEndian(&m_Data, 8, &iStateShift);
		if (bSuccess) {
			m_StartSample = m_Buffer->at(0).GetStart();
			m_EndSample = m_Buffer->at(iStateShift - 1).GetEnd();
			m_Buffer->finishedReading(iStateShift);
			m_State = finished;
		}
		else {
			m_State = failed;
		}
	}
}

void Checksumme::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>&  resultStrings)
{
	char numberString[64];
	AnalyzerHelpers::GetNumberString(data1, *display, 8, numberString, 64);
	resultStrings[0] = "Checksumme";
	resultStrings[1] = ((std::string)"Checksumme: ") + numberString;
	resultStrings[2] = resultStrings[1];
}
