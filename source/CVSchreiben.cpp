#include "CVSchreiben.h"



CVSchreiben::CVSchreiben(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer,buffer,"CV Schreiben"),
	m_pattern(timer,buffer)
{
	m_pattern.SetPattern(0b001, 3);
}


CVSchreiben::~CVSchreiben()
{
}

void CVSchreiben::Parse()
{
	m_State = busy;
	m_StartSample = m_Buffer->at(0).GetStart();
	if (m_Buffer->countReadable() >= 54) {
		if (m_pattern.IsNextPattern()) {
			m_Buffer->finishedReading(m_pattern.GetLength());
			int iStateshift = 0;
			bool bSuccess = m_Buffer->readBitsBigEndian(&m_Data, 26, &iStateshift);
			if (bSuccess) {
				m_State = finished;
				m_EndSample = m_Buffer->at(iStateshift - 1).GetEnd();
			}
			else
				m_State = failed;
		}
		else
			m_State = failed;
	}
}

void CVSchreiben::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
	char vNumberString[64];
	char iNumberString[64];
	char iByteString[64];

	AnalyzerHelpers::GetNumberString((data1&m_MaskV) >> 16, *display, 10, vNumberString, 64);
	AnalyzerHelpers::GetNumberString((data1&m_MaskI) >> 10, *display, 6, iNumberString, 64);
	AnalyzerHelpers::GetNumberString((data1&m_MaskD), *display, 8, iByteString, 64);

	resultStrings[0] = "CV Schreiben";
	resultStrings[1] = (std::string)"CV Schreiben: CV=" + vNumberString + ", I=" + iNumberString + ", byte=" + iByteString;
	resultStrings[2] = (std::string)"CV Schreiben: CV-Nummer=" + vNumberString + ", Index=" + iNumberString + ", byte=" + iByteString;
}
