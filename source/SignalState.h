#pragma once

#include <LogicPublicTypes.h>
#include "SignalTimer.h"

class SignalState
{
public:
	SignalState();
	SignalState(U64 start, U64 end, SignalTimer::eTimer rounded);
	~SignalState();

	void Set(U64 start, U64 end);
	void Set(U64 start, U64 end, SignalTimer::eTimer rounded);
	U64 GetDurationInSamples() const;
	U64 GetStart() const;
	U64 GetEnd() const;
	bool GetRounded(SignalTimer::eTimer& out) const;

private:
	U64 m_StartSample;
	U64 m_EndSample;
	SignalTimer::eTimer m_rounded;
	bool m_RoundedIsWellDefined;
};