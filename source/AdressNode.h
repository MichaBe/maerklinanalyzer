#pragma once

#include "ParseNode.h"
#include "FramePattern.h"
#include <AnalyzerHelpers.h>
#include <string>

class AdressNode :
	public ParseNode
{
public:
	AdressNode(SignalTimer* timer, Ringspeicher* buffer);
	~AdressNode();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_AdressStart7Bit;
	FramePattern m_AdressStart9Bit;
	FramePattern m_AdressStart11Bit;
	FramePattern m_AdressStart14Bit;
};

