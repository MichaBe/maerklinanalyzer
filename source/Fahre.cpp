#include "Fahre.h"



Fahre::Fahre(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "Fahre"),
	m_Kurz(timer,buffer),
	m_Lang(timer,buffer)
{
	m_Kurz.SetPattern((U32)0b000, 3);
	m_Lang.SetPattern(0b001, 3);
	Clear();
}


Fahre::~Fahre()
{
}

void Fahre::Parse()
{
	m_State = busy;
	if (m_Buffer->countReadable() >= 20) {
		int iStateShift = 0;
		int readBits = 0;
		if (m_Kurz.IsNextPattern()) {
			iStateShift = m_Kurz.GetLength();
			readBits = 4;
			m_Type = 1;
		}
		else if (m_Lang.IsNextPattern()) {
			iStateShift = m_Lang.GetLength();
			readBits = 8;
			m_Type = 2;
		}
		else
			m_State = failed;

		if (m_State == busy) {
			int iStates = 0;
			bool bSuccess = m_Buffer->readBitsBigEndian(&m_Data, readBits, &iStates, iStateShift);
			
			if (bSuccess) {
				iStateShift += iStates;
				m_StartSample = m_Buffer->at(0).GetStart();
				m_EndSample = m_Buffer->at(iStateShift-1).GetEnd();
				m_Buffer->finishedReading(iStateShift);
				m_State = finished;
			}
			else
				m_State = failed;
		}
	}
}


void Fahre::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
	int fahrstufe;
	bool rueckwaerts;
	if (type == 1) {
		//Fahren (kurz)
		resultStrings[0] = "Fahren (kurz)";
		fahrstufe = (data1 & 0b0111) * 16;
		rueckwaerts = data1 & 0b01000;
	}
	else {
		//Fahren
		resultStrings[0] = "Fahren";
		fahrstufe = (data1 & 0b01111111);
		rueckwaerts = data1 & 0b010000000;
	}
	resultStrings[1] = resultStrings[0];
	if (fahrstufe == 1) {
		resultStrings[1] +=  ": Notstopp";
		resultStrings[2] = resultStrings[1];
	}
	else if (fahrstufe == 0) {
		resultStrings[1] +=  ": Halt";
		resultStrings[2] = resultStrings[1];
	}
	else {
		if (rueckwaerts)
			resultStrings[1] += ": rueckw.";
		else
			resultStrings[1] += ": vorw.";

		resultStrings[2] = resultStrings[1] + ", Fahrstufe " + std::to_string(fahrstufe);
	}
}
