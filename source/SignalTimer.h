#pragma once

#include <LogicPublicTypes.h>
#include <utility>

class SignalTimer
{
public:
	enum eTimer {m25, m50, m100, m456, m912, m6400};

	SignalTimer(U32 sampleRate, float tolerance);
	~SignalTimer();

	void SetSamplerateAndTolerance(U32 sampleRate, float tolerancePercentage);
	bool GetRoundedSignalType(U64 sampleCount, eTimer& outTimer);
	U64 at(eTimer n);
	U32 getSamplerate();

private:
	float m_Tolerance;
	const int m_TimerLength = 6;
	U32 m_SampleRate;
	U64* m_SampleCount;
	U64* m_SampleCountUpperBound;
	U64* m_SampleCountLowerBound;
};