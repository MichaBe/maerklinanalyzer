#pragma once

#include <AnalyzerChannelData.h>

#include "MFXAnalyzerResults.h"

#include "Ringspeicher.h"
#include "Ergebnisspeicher.h"
#include "SignalState.h"
#include "FramePattern.h"
#include "SignalTimer.h"
#include "ParseNode.h"
#include "Reserviert.h"
#include "RootNode.h"
#include "DataframeStartNode.h"
#include "AdressNode.h"
#include "Fahre.h"
#include "Funktionen.h"
#include "KonfigurationStart.h"
#include "CVLesen.h"
#include "RueckmeldungStart.h"
#include "MehrbitRueckmeldung.h"
#include "EinbitRueckmeldung.h"
#include "FrameEnde.h"
#include "Checksumme.h"
#include "CVSchreiben.h"
#include "Dekodersuche.h"
#include "AdressenZuweisung.h"
#include "Existenzabfrage.h"
#include "Zentrale.h"
#include "BitDecision.h"

#include <list>
#include <map>
#include <string>

class Parser
{
public:
	Parser(Ringspeicher* buffer, SignalTimer* timer, AnalyzerChannelData* channel, MFXAnalyzerResults* results);
	~Parser();

	U64 ParseSignal();
	void GetResultString(Frame* frame, DisplayBase* display, std::array<std::string, 3>& resultStrings);
	std::string GetExportString(Frame* frame, DisplayBase* display, U64 triggerSample, char seperator = ';');
	

private:
	void FillBuffer();

	std::map<int, ParseNode*> m_NodeToID;

	ParseNode* m_CurrentNode;
	std::list<ParseNode*> m_PossibleNexts;
	ParseNode* m_InitialNode;

	Ergebnisspeicher m_FrameBuffer;
	Ringspeicher* m_Buffer;
	SignalTimer* m_Timer;
	AnalyzerChannelData* m_ChannelGleis;
	MFXAnalyzerResults* m_Results;

	ParseNode* m_Root;
	ParseNode* m_DataFrameStart;
	ParseNode* m_AdressNode;
	ParseNode* m_BitDecision_Adresse;//TODO testen
	ParseNode* m_Fahre;
	ParseNode* m_Funktion;
	ParseNode* m_KonfigurationsStart;
	ParseNode* m_Reserviert_Command101;
	ParseNode* m_Reserviert_Command110;
	ParseNode* m_BitDecision_Konfiguration;//TODO testen
	ParseNode* m_CVLesen;
	ParseNode* m_CVSchreiben;//TODO testen
	ParseNode* m_DekoderSuche;
	ParseNode* m_Adresszuweisung;
	ParseNode* m_Existenzabfrage;
	ParseNode* m_Zentrale;
	ParseNode* m_Reserviert_Config110;
	ParseNode* m_Reserviert_Config111;
	ParseNode* m_RueckmeldungStart;
	ParseNode* m_MehrbitRueckmeldung;
	ParseNode* m_EinbitRueckmeldung;
	ParseNode* m_FrameEnde;
	ParseNode* m_Checksumme;

	FramePattern m_reserviert101;
	FramePattern m_reserviert110;
	FramePattern m_reserviert111;

	U64 m_CurrentSample;
	int m_CurrentStuffingCounter;
};