#include "Zentrale.h"



Zentrale::Zentrale(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer,buffer, "Zentrale"),
	m_StartPattern(timer,buffer)
{
	m_StartPattern.SetPattern(0b101, 3);
}


Zentrale::~Zentrale()
{
}

void Zentrale::Parse()
{
	m_State = busy;
	m_StartSample = m_Buffer->at(0).GetStart();
	if (m_Buffer->countReadable() >= 101) {
		if (m_StartPattern.IsNextPattern()) {
			m_Buffer->finishedReading(m_StartPattern.GetLength());
			int iStateShift = 0;
			bool bSuccess = m_Buffer->readBitsBigEndian(&m_Data, 48, &iStateShift);

			if (bSuccess) {
				m_EndSample = m_Buffer->at(iStateShift - 1).GetEnd();
				m_Buffer->finishedReading(iStateShift);
				m_State = finished;
			}
			else
				m_State = failed;
		}
		else
			m_State = failed;
	}
}

void Zentrale::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
	char uidNumberString[64];
	char zaehlerNumberString[64];

	U32 UID = (data1&m_uidMask) >> 32;
	U16 zaehler = (data1&m_zMask);

	AnalyzerHelpers::GetNumberString(UID, *display, 32, uidNumberString, 64);
	AnalyzerHelpers::GetNumberString(zaehler, *display, 16, zaehlerNumberString, 64);

	resultStrings[0] = "Zentrale";
	resultStrings[1] = (std::string)"Zentrale: UID=" + uidNumberString + ", Zaehler=" + zaehlerNumberString;
	resultStrings[2] = (std::string)"Zentrale: UID=" + uidNumberString + ", Neuanmeldezaehler=" + zaehlerNumberString;
}
