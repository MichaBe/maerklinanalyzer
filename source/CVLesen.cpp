#include "CVLesen.h"



CVLesen::CVLesen(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "CV Lesen"),
	m_pattern(timer, buffer)
{
	m_pattern.SetPattern((U32)0b000, 3);
	Clear();
}


CVLesen::~CVLesen()
{
}

void CVLesen::Parse()
{
	m_State = busy;
	if (m_Buffer->countReadable() >= 18 * 2 + 3) {
		if (m_pattern.IsNextPattern()) {
			m_StartSample = m_Buffer->at(0).GetStart();
			int iOutStateCount = 0;
			bool bSuccess = m_Buffer->readBitsBigEndian(&m_Data, 21, &iOutStateCount);
			if (bSuccess) {
				m_EndSample = m_Buffer->at(iOutStateCount - 1).GetEnd();
				m_Buffer->finishedReading(iOutStateCount);
				m_State = finished;
			}
			else
				m_State = failed;
		}
		else
			m_State = failed;
	}
}

void CVLesen::Clear()
{
	ParseNode::Clear();

}

void CVLesen::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
	resultStrings[0] = "CV Lesen";

	char vNumberString[64];
	char iNumberString[64];
	U64 curRead = ((data1&m_MaskV) >> 8);
	AnalyzerHelpers::GetNumberString(curRead, *display, 10, vNumberString, 64);
	curRead = ((data1&m_MaskI) >> 2);
	AnalyzerHelpers::GetNumberString(curRead, *display, 6, iNumberString, 64);
	curRead = data1&m_MaskC;
	int iByteCount = 1;
	for (int i = 0; i < curRead; ++i)
		iByteCount *= 2;

	resultStrings[1] = resultStrings[0] + ": CV=" + vNumberString + ", I=" + iNumberString;
	resultStrings[2] = resultStrings[0] + ": CV-Nummer=" + vNumberString + ", Index=" + iNumberString + ", Anz. Bytes bei Rueckmeldung=" + std::to_string(iByteCount);
}
