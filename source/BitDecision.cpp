#include "BitDecision.h"



BitDecision::BitDecision(SignalTimer* timer, Ringspeicher* buffer, int bitLength)
	: ParseNode(timer, buffer, "BitDecision "+std::to_string(bitLength)+" bit")
{
	Clear();
	m_bitLength = bitLength;
}


BitDecision::~BitDecision()
{
}

void BitDecision::Parse()
{
	m_State = busy;
	if (m_Buffer->countReadable() >= m_bitLength * 2) {
		int iStateCount = 0;
		bool bSuccess = m_Buffer->readBitsBigEndian(&m_readBits, m_bitLength, &iStateCount);
		if (bSuccess && m_nextNodes.count(m_readBits) == 1) {
			m_State = finished_no_frame;
		}
		else {
			m_State = failed;
		}
	}
}

void BitDecision::Clear()
{
	ParseNode::Clear();
	m_readBits = 0;
}

std::list<ParseNode*>* BitDecision::GetNext()
{
	return &(m_nextNodes.at(m_readBits));
}

void BitDecision::SetNext(U64 bitMuster, ParseNode * next)
{
	m_nextNodes[bitMuster] = std::list<ParseNode*>{ next };
}
