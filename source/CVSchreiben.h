#pragma once

#include "ParseNode.h"
#include "FramePattern.h"
#include "AnalyzerHelpers.h"

class CVSchreiben :
	public ParseNode
{
public:
	CVSchreiben(SignalTimer* timer, Ringspeicher* buffer);
	~CVSchreiben();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_pattern;
	const U32 m_MaskV = 0x3FF0000;
	const U32 m_MaskI = 0xFC00;
	const U32 m_MaskD = 0xFF;
};

