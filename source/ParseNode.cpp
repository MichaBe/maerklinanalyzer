#include "ParseNode.h"

int ParseNode::m_ID = 0;

ParseNode::ParseNode(SignalTimer* timer, Ringspeicher* buffer, std::string name)
{
	m_NodeName = name;
	m_Timer = timer;
	m_Buffer = buffer;
	m_State = idle;
	m_ID++;
	m_NodeID = m_ID;
	m_StartSample = m_EndSample = 0;
}


ParseNode::~ParseNode()
{
}

bool ParseNode::GenerateFrame(Frame * frame)
{
	frame->mFlags = 0;
	frame->mData1 = m_NodeID;
	frame->mData2 = m_Data;
	frame->mType = m_Type;
	frame->mStartingSampleInclusive = m_StartSample;
	frame->mEndingSampleInclusive = m_EndSample;
	return (m_State == finished);
}

void ParseNode::Clear()
{
	m_State = idle;
	m_StartSample = m_EndSample = 0;
	m_Data = 0;
	m_Type = 0;
}

void ParseNode::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
}

std::list<ParseNode*>* ParseNode::GetNext()
{
	return &m_Next;
}

void ParseNode::AddNext(ParseNode* next) {
	m_Next.push_back(next);
}

ParseNode::state ParseNode::GetState()
{
	return m_State;
}

int ParseNode::GetID()
{
	return m_NodeID;
}

std::string ParseNode::GetName()
{
	return m_NodeName;
}
