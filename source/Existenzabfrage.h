#pragma once

#include "ParseNode.h"
#include "FramePattern.h"
#include "AnalyzerHelpers.h"

class Existenzabfrage :
	public ParseNode
{
public:
	Existenzabfrage(SignalTimer* timer, Ringspeicher* buffer);
	~Existenzabfrage();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_Startpattern;

};

