#pragma once

#include "ParseNode.h"
#include "AnalyzerHelpers.h"
#include <string>

class Checksumme :
	public ParseNode
{
public:
	Checksumme(SignalTimer* timer, Ringspeicher* buffer);
	~Checksumme();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
};

