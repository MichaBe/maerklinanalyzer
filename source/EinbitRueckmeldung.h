#pragma once

#include "ParseNode.h"
#include "FramePattern.h"

class EinbitRueckmeldung :
	public ParseNode
{
public:
	EinbitRueckmeldung(SignalTimer* timer, Ringspeicher* buffer);
	~EinbitRueckmeldung();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_HalfSync;
};

