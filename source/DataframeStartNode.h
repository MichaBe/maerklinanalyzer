#pragma once

#include "ParseNode.h"
#include "FramePattern.h"
#include "AnalyzerTypes.h"

class DataframeStartNode :
	public ParseNode
{
public:
	DataframeStartNode(SignalTimer* timer, Ringspeicher* buffer);
	~DataframeStartNode();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_Sync;

};

