#pragma once

#include "ParseNode.h"
#include "FramePattern.h"

class RueckmeldungStart :
	public ParseNode
{
public:
	RueckmeldungStart(SignalTimer* timer, Ringspeicher* buffer);
	~RueckmeldungStart();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_StartSync;
	FramePattern m_StartHalfSync;
};

