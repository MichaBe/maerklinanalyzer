#include "RueckmeldungStart.h"



RueckmeldungStart::RueckmeldungStart(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "Rueckmeldungsstart"),
	m_StartSync(timer, buffer),
	m_StartHalfSync(timer, buffer)
{
	Clear();
	SignalTimer::eTimer mS = SignalTimer::m50;
	SignalTimer::eTimer mL = SignalTimer::m100;
	m_StartSync.SetPattern(new SignalTimer::eTimer[12]{ mL,mS,mL,mL,mS,mL,mL,mL,mS,mS,mS,mS }, 12);
	m_StartHalfSync.SetPattern(new SignalTimer::eTimer[9]{ mL,mS,mL,mL,mL,mS,mS,mS,mS }, 9);

}


RueckmeldungStart::~RueckmeldungStart()
{
}

void RueckmeldungStart::Parse()
{
	m_State = busy;
	if (m_Buffer->countReadable() >= 12) {
		if (m_StartSync.IsNextPattern()) {
			m_StartSample = m_Buffer->at(0).GetStart();
			m_EndSample = m_Buffer->at(m_StartSync.GetLength() - 1).GetEnd();
			m_Buffer->finishedReading(m_StartSync.GetLength());
			m_State = finished;
		}
		else if (m_StartHalfSync.IsNextPattern()) {
			m_StartSample = m_Buffer->at(0).GetStart();
			m_EndSample = m_Buffer->at(m_StartHalfSync.GetLength() - 1).GetEnd();
			m_Buffer->finishedReading(m_StartHalfSync.GetLength());
			m_State = finished;
		}
		else {
			m_State = failed;
		}
	}
}

void RueckmeldungStart::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
	for(int i = 0; i < 3; ++i)
		resultStrings[i] = "Rueckmeldungsstart";
}
