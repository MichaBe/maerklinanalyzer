#include "Dekodersuche.h"



Dekodersuche::Dekodersuche(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "Dekodersuche"),
	m_StartPattern(timer,buffer)
{
	m_StartPattern.SetPattern(0b010, 3);
}


Dekodersuche::~Dekodersuche()
{
}

void Dekodersuche::Parse()
{
	m_State = busy;
	m_StartSample = m_Buffer->at(0).GetStart();
	if (m_Buffer->countReadable() >= 60) {
		if (m_StartPattern.IsNextPattern()) {
			m_Buffer->finishedReading(m_StartPattern.GetLength());
			int iStateShift = 0;
			bool bSuccess = m_Buffer->readBitsBigEndian(&m_Data, 38, &iStateShift);
			if (bSuccess) {
				m_EndSample = m_Buffer->at(iStateShift - 1).GetEnd();
				m_Buffer->finishedReading(iStateShift);
				m_State = finished;
			}
			else
				m_State = failed;
		}
		else
			m_State = failed;
	}
}

void Dekodersuche::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
	U8 c = (data1&m_cMask) >> 32;

	resultStrings[0] = "Dekodersuche";
	if (c == 0) {
		resultStrings[1] = "Dekodersuche: Broadcast";
		resultStrings[2] = resultStrings[1];
	}
	else {
		char uNumberString[64];
		AnalyzerHelpers::GetNumberString((data1&m_uMask) >> (32 - c), DisplayBase::Binary, c, uNumberString, 64);
		resultStrings[1] = "Dekodersuche: " + std::to_string(c) + " Bits spezifiziert";
		resultStrings[2] = (std::string)"Dekodersuche: " + uNumberString + "+" + std::to_string(32 - c) + " Bits";
	}
}
