#include "Ergebnisspeicher.h"



Ergebnisspeicher::Ergebnisspeicher()
{
	m_curFrameID = 0;
}


Ergebnisspeicher::~Ergebnisspeicher()
{
}

void Ergebnisspeicher::AddNode(Frame * f)
{
	if (m_Frames[m_curFrameID].nodes.size() == 0) {
		//erstes frame dieser entity
		m_Frames[m_curFrameID].startSample = f->mStartingSampleInclusive;
	}
	m_Frames[m_curFrameID].endSample = f->mEndingSampleInclusive;
	m_Frames[m_curFrameID].nodes.push_back(NodeInfo(f->mData1, f->mData2, f->mType, f->mEndingSampleInclusive));
}

bool Ergebnisspeicher::FinishedFrame(U64* outFrameID)
{
	bool bReturner = false;
	if (m_Frames[m_curFrameID].nodes.size() != 0) {
		*outFrameID = m_curFrameID;
		m_curFrameID++;
		bReturner = true;
	}
	return bReturner;
}

Ergebnisspeicher::Entity* Ergebnisspeicher::GetFrame(U64 frameID)
{
	return &(m_Frames.at(frameID));
}

int Ergebnisspeicher::GetCurNodeCount()
{
	return m_Frames[m_curFrameID].nodes.size();
}
