#pragma once

#include "LogicPublicTypes.h"
#include "AnalyzerResults.h"
#include <list>
#include <map>

class Ergebnisspeicher
{
public:
	struct NodeInfo {
		NodeInfo(int pId, U64 pData, U8 pType, U64 pEndsample)
			: nodeID(pId),data(pData),type(pType),endSample(pEndsample)
		{}
		int nodeID;
		U64 data;
		U8 type;
		U64 endSample;
	};
	struct Entity {
		Entity()
			: startSample(0), endSample(0) 
		{}
		U64 startSample;
		U64 endSample;
		std::list<NodeInfo> nodes;
	};

	Ergebnisspeicher();
	~Ergebnisspeicher();

	void AddNode(Frame* f);
	bool FinishedFrame(U64* outFrameID);
	Entity* GetFrame(U64 frameID);
	int GetCurNodeCount();

private:
	std::map<U64, Entity> m_Frames;
	U64 m_curFrameID;
};