#pragma once

#include "ParseNode.h"
#include "FramePattern.h"
#include "AnalyzerHelpers.h"

class Dekodersuche :
	public ParseNode
{
public:
	Dekodersuche(SignalTimer* timer, Ringspeicher* buffer);
	~Dekodersuche();

	void Parse();
	void GetResultString(U64 data1, U8 type, DisplayBase* display, std::array<std::string, 3>& resultStrings);

private:
	FramePattern m_StartPattern;
	const U64 m_cMask = 0x3F00000000;
	const U64 m_uMask = 0xFFFFFFFF;

};

