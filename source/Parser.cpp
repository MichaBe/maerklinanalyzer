#include "Parser.h"

Parser::Parser(Ringspeicher* buffer, SignalTimer* timer, AnalyzerChannelData* channel, MFXAnalyzerResults* results)
	: m_reserviert101(timer, buffer),
	m_reserviert110(timer, buffer),
	m_reserviert111(timer, buffer)
{
	m_Buffer = buffer;
	m_Timer = timer;
	m_ChannelGleis = channel;
	m_Results = results;
	
	m_reserviert101.SetPattern(0b101, 3);
	m_reserviert110.SetPattern(0b110, 3);
	m_reserviert111.SetPattern(0b111, 3);

	m_Root = new RootNode(timer, buffer);
	m_NodeToID[m_Root->GetID()] = m_Root;

	m_DataFrameStart = new DataframeStartNode(timer, buffer);
	m_NodeToID[m_DataFrameStart->GetID()] = m_DataFrameStart;

	m_AdressNode = new AdressNode(timer, buffer);
	m_NodeToID[m_AdressNode->GetID()] = m_AdressNode;
	
	m_BitDecision_Adresse = new BitDecision(timer, buffer, 3); //TODO testen
	m_NodeToID[m_BitDecision_Adresse->GetID()] = m_BitDecision_Adresse;

	m_Fahre = new Fahre(timer, buffer);
	m_NodeToID[m_Fahre->GetID()] = m_Fahre;

	m_Funktion = new Funktionen(timer, buffer);
	m_NodeToID[m_Funktion->GetID()] = m_Funktion;

	m_KonfigurationsStart = new KonfigurationStart(timer, buffer);
	m_NodeToID[m_KonfigurationsStart->GetID()] = m_KonfigurationsStart;

	m_Reserviert_Command101 = new Reserviert(timer, buffer, &m_reserviert101, "101");
	m_NodeToID[m_Reserviert_Command101->GetID()] = m_Reserviert_Command101;

	m_Reserviert_Command110 = new Reserviert(timer, buffer, &m_reserviert110, "110");
	m_NodeToID[m_Reserviert_Command110->GetID()] = m_Reserviert_Command110;

	m_BitDecision_Konfiguration = new BitDecision(timer, buffer, 3);
	m_NodeToID[m_BitDecision_Konfiguration->GetID()] = m_BitDecision_Konfiguration;

	m_CVLesen = new CVLesen(timer, buffer);
	m_NodeToID[m_CVLesen->GetID()] = m_CVLesen;

	m_CVSchreiben = new CVSchreiben(timer, buffer);
	m_NodeToID[m_CVSchreiben->GetID()] = m_CVSchreiben;

	m_DekoderSuche = new Dekodersuche(timer, buffer);
	m_NodeToID[m_DekoderSuche->GetID()] = m_DekoderSuche;

	m_Adresszuweisung = new AdressenZuweisung(timer, buffer);
	m_NodeToID[m_Adresszuweisung->GetID()] = m_Adresszuweisung;
	
	m_Existenzabfrage = new Existenzabfrage(timer, buffer);
	m_NodeToID[m_Existenzabfrage->GetID()] = m_Existenzabfrage;

	m_Zentrale = new Zentrale(timer, buffer);
	m_NodeToID[m_Zentrale->GetID()] = m_Zentrale;
	
	m_Reserviert_Config110 = new Reserviert(timer, buffer, &m_reserviert110, "110");
	m_NodeToID[m_Reserviert_Config110->GetID()] = m_Reserviert_Config110;

	m_Reserviert_Config111 = new Reserviert(timer, buffer, &m_reserviert111, "111");
	m_NodeToID[m_Reserviert_Config111->GetID()] = m_Reserviert_Config111;

	m_RueckmeldungStart = new RueckmeldungStart(timer, buffer);
	m_NodeToID[m_RueckmeldungStart->GetID()] = m_RueckmeldungStart;

	m_MehrbitRueckmeldung = new MehrbitRueckmeldung(timer, buffer);
	m_NodeToID[m_MehrbitRueckmeldung->GetID()] = m_MehrbitRueckmeldung;

	m_EinbitRueckmeldung = new EinbitRueckmeldung(timer, buffer);
	m_NodeToID[m_EinbitRueckmeldung->GetID()] = m_EinbitRueckmeldung;

	m_FrameEnde = new FrameEnde(timer, buffer);
	m_NodeToID[m_FrameEnde->GetID()] = m_FrameEnde;

	m_Checksumme = new Checksumme(timer, buffer);
	m_NodeToID[m_Checksumme->GetID()] = m_Checksumme;

	((RootNode*)m_Root)->SetNext(m_DataFrameStart, m_RueckmeldungStart);
	
	m_DataFrameStart->AddNext(m_AdressNode);

	m_RueckmeldungStart->AddNext(m_MehrbitRueckmeldung);
	m_RueckmeldungStart->AddNext(m_EinbitRueckmeldung);

	m_AdressNode->AddNext(m_BitDecision_Adresse); //TODO testen

	BitDecision* temp = (BitDecision*)m_BitDecision_Adresse; //TODO testen
	temp->SetNext(0b000, m_Fahre);
	temp->SetNext(0b001, m_Fahre);
	temp->SetNext(0b010, m_Funktion);
	temp->SetNext(0b011, m_Funktion);
	temp->SetNext(0b100, m_Funktion);
	temp->SetNext(0b101, m_Reserviert_Command101);
	temp->SetNext(0b110, m_Reserviert_Command110);
	temp->SetNext(0b111, m_KonfigurationsStart);

	m_Fahre->AddNext(m_FrameEnde);
	m_Funktion->AddNext(m_FrameEnde);

	m_KonfigurationsStart->AddNext(m_BitDecision_Konfiguration);

	temp = (BitDecision*)m_BitDecision_Konfiguration; //TODO testen
	temp->SetNext(0b000, m_CVLesen);
	temp->SetNext(0b001, m_CVSchreiben);
	temp->SetNext(0b010, m_DekoderSuche);
	temp->SetNext(0b011, m_Adresszuweisung);
	temp->SetNext(0b100, m_Existenzabfrage);
	temp->SetNext(0b101, m_Zentrale);
	temp->SetNext(0b110, m_Reserviert_Config110);
	temp->SetNext(0b111, m_Reserviert_Config111);
	
	m_Reserviert_Command101->AddNext(m_FrameEnde);
	m_Reserviert_Command110->AddNext(m_FrameEnde);

	m_CVLesen->AddNext(m_FrameEnde);
	m_CVSchreiben->AddNext(m_FrameEnde);
	m_DekoderSuche->AddNext(m_FrameEnde);
	m_Adresszuweisung->AddNext(m_FrameEnde);
	m_Existenzabfrage->AddNext(m_FrameEnde);
	m_Zentrale->AddNext(m_FrameEnde);
	m_Reserviert_Config110->AddNext(m_FrameEnde);
	m_Reserviert_Config111->AddNext(m_FrameEnde);

	((FrameEnde*)m_FrameEnde)->SetNext(m_Checksumme, m_AdressNode->GetNext());

	m_CurrentNode = nullptr;
	m_InitialNode = m_Root;
	m_PossibleNexts.push_back(m_InitialNode);
	m_CurrentStuffingCounter = 0;
}


Parser::~Parser()
{
	delete m_Root;
	delete m_DataFrameStart;
	delete m_AdressNode;
	delete m_BitDecision_Adresse;
	delete m_Fahre;
	delete m_Funktion;
	delete m_KonfigurationsStart;
	delete m_Reserviert_Command101;
	delete m_Reserviert_Command110;
	delete m_BitDecision_Konfiguration;
	delete m_CVLesen;
	delete m_CVSchreiben;
	delete m_DekoderSuche;
	delete m_Adresszuweisung;
	delete m_Existenzabfrage;
	delete m_Zentrale;
	delete m_Reserviert_Config110;
	delete m_Reserviert_Config111;
	delete m_RueckmeldungStart;
	delete m_MehrbitRueckmeldung;
	delete m_FrameEnde;
	delete m_Checksumme;
}

U64 Parser::ParseSignal()
{
	//Laden
	FillBuffer();
		
	if (m_PossibleNexts.size() != 0) {
		m_CurrentNode = m_PossibleNexts.front();
		m_PossibleNexts.pop_front();
	}
	else {
		U64 id = 0;
		bool bResult = m_FrameBuffer.FinishedFrame(&id);
		if (bResult) {
			Ergebnisspeicher::Entity* frameInfo = m_FrameBuffer.GetFrame(id);
			Frame f;
			f.mData1 = id;
			f.mStartingSampleInclusive = frameInfo->startSample;
			f.mEndingSampleInclusive = frameInfo->endSample;
			m_Results->AddFrame(f);
			m_Results->CommitResults();
			m_CurrentSample = f.mEndingSampleInclusive;
		}
		m_CurrentNode = m_InitialNode;
	}
	
	//Parsen
	m_CurrentNode->Parse();
	int i = 0;
	while (m_CurrentNode->GetState() == ParseNode::busy && i < 4) { //5 durchläufe insgesamt (1x vor schleife, 4x in schleife)
		FillBuffer();
		m_CurrentNode->Parse();
		++i;
	}

	//Aufräumen
	if (m_CurrentNode->GetState() == ParseNode::finished || m_CurrentNode->GetState() == ParseNode::finished_no_frame) {
		m_PossibleNexts.clear();
		for (auto it : (*m_CurrentNode->GetNext())) {
			m_PossibleNexts.push_back(it);
		}
		Frame frame;
		bool generatedFrame = m_CurrentNode->GenerateFrame(&frame);
		if (generatedFrame) {
			m_FrameBuffer.AddNode(&frame);
		}
	}
	m_CurrentNode->Clear();
	return m_CurrentSample;
}

void Parser::GetResultString(Frame* frame, DisplayBase* display, std::array<std::string, 3>& resultStrings)
{
	Ergebnisspeicher::Entity* frameInfo = m_FrameBuffer.GetFrame(frame->mData1);
	std::string resultstrings[3];
	std::array<std::string, 3>  tempNodeResult;
	for (auto it : frameInfo->nodes) {
		m_NodeToID.at(it.nodeID)->GetResultString(it.data, it.type, display, tempNodeResult);
		for (int i = 0; i < 3; ++i) {
			resultstrings[i] = resultstrings[i] + tempNodeResult[i];
			resultstrings[i] = resultstrings[i] + ", ";
		}
	}
	for (int i = 0; i < 3; ++i) {
		resultstrings[i] = resultstrings[i].substr(0, resultstrings[i].size() - 2);
		resultStrings[i] = resultstrings[i];
	}
}

std::string Parser::GetExportString(Frame * frame, DisplayBase * display, U64 triggerSample, char seperator)
{
	std::string exportString;
	Ergebnisspeicher::Entity* frameInfo = m_FrameBuffer.GetFrame(frame->mData1);
	U64 startSample = frameInfo->startSample;
	U32 sampleRate = m_Timer->getSamplerate();
	std::array<std::string, 3>  resultString;
	char resultTimeString[128];
	for (auto it : frameInfo->nodes) {
		m_NodeToID.at(it.nodeID)->GetResultString(it.data, it.type, display, resultString);
		AnalyzerHelpers::GetTimeString(startSample, triggerSample, sampleRate, resultTimeString, 128);
		exportString = exportString + "\n"
			+ resultTimeString + seperator
			+ std::to_string(it.nodeID) + seperator
			+ m_NodeToID.at(it.nodeID)->GetName() + seperator
			+ resultString[2];

		startSample = it.endSample + 1;
	}
	return exportString;
}

void Parser::FillBuffer() {
	SignalTimer::eTimer tempRoundedTime;
	bool bSuccess;
	U64 tempStart, tempEnd;
	while (m_Buffer->countWriteable() != 0) {
		tempStart = m_ChannelGleis->GetSampleNumber() + 1;
		m_ChannelGleis->AdvanceToNextEdge();
		tempEnd = m_ChannelGleis->GetSampleNumber();
		bSuccess = m_Timer->GetRoundedSignalType(tempEnd - tempStart, tempRoundedTime);
		if (bSuccess) {
			//Stuffingbits überspringen (die null (m100) nach 8 übertragenen einsen (16*m50) ist ein stuffingbit)
			if (m_CurrentStuffingCounter == 16 && tempRoundedTime == SignalTimer::eTimer::m100) {
				m_CurrentStuffingCounter = 0;
			}
			else {
				if (tempRoundedTime == SignalTimer::eTimer::m50)
					++m_CurrentStuffingCounter;
				else
					m_CurrentStuffingCounter = 0;
				m_Buffer->write().Set(tempStart, tempEnd, tempRoundedTime);
			}
		}
		else {
			m_Buffer->write().Set(tempStart, tempEnd);
		}
	}
}