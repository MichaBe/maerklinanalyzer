#include "KonfigurationStart.h"


KonfigurationStart::KonfigurationStart(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer,buffer, "Konfigurationsstart"),
	m_pattern(timer,buffer)
{
	m_pattern.SetPattern(0b111, 3);
	Clear();
}

void KonfigurationStart::Parse()
{
	m_State = busy;
	if (m_Buffer->countReadable() >= 6) {
		if (m_pattern.IsNextPattern()) {
			m_State = finished;
			m_StartSample = m_Buffer->at(0).GetStart();
			m_EndSample = m_Buffer->at(m_pattern.GetLength() - 1).GetEnd();
			m_Buffer->finishedReading(m_pattern.GetLength());
		}
		else
			m_State = failed;
	}
}

void KonfigurationStart::Clear()
{
	ParseNode::Clear();
}

void KonfigurationStart::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
	resultStrings[0] = resultStrings[1] = resultStrings[2] = "Konfiguration";
}


KonfigurationStart::~KonfigurationStart()
{
}
