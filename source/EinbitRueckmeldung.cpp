#include "EinbitRueckmeldung.h"



EinbitRueckmeldung::EinbitRueckmeldung(SignalTimer* timer, Ringspeicher* buffer)
	: ParseNode(timer, buffer, "Einbitrueckmeldung"),
	m_HalfSync(timer, buffer)
{
	SignalTimer::eTimer l = SignalTimer::m100;
	SignalTimer::eTimer k = SignalTimer::m50;
	m_HalfSync.SetPattern(new SignalTimer::eTimer[3]{ l,k,l }, 3);
}


EinbitRueckmeldung::~EinbitRueckmeldung()
{
}

void EinbitRueckmeldung::Parse()
{
	m_State = busy;
	if (m_Buffer->countReadable() >= (2 + m_HalfSync.GetLength()*3 + 2)) {
		int iStateshift = 0;
		for (int i = 0; i < 2; ++i) {
			SignalTimer::eTimer test;
			bool bValid = m_Buffer->at(i).GetRounded(test);
			if (bValid && test == SignalTimer::m6400) {
				m_StartSample = m_Buffer->at(i).GetStart();
				iStateshift = i+1;
				break;
			}
		}
		int halfSyncPatternCount = 0;
		while (m_HalfSync.IsNextPattern(iStateshift)) {
			halfSyncPatternCount++;
			iStateshift += m_HalfSync.GetLength();
		}
		if(halfSyncPatternCount > 1) {
			for (int i = 0; i < 2; ++i) {
				SignalTimer::eTimer test;
				bool bValid = m_Buffer->at(iStateshift + i).GetRounded(test);
				if (bValid && test == SignalTimer::m6400) {
					iStateshift = iStateshift + i + 1;
					m_EndSample = m_Buffer->at(iStateshift - 1).GetEnd();
					m_State = finished;
				}
			}
		}
		if (m_State != finished) {
			m_State = failed;
		}
	}
}

void EinbitRueckmeldung::GetResultString(U64 data1, U8 type, DisplayBase * display, std::array<std::string, 3>& resultStrings)
{
	resultStrings[0] = resultStrings[1] = resultStrings[2] = "Einbitrueckmeldung";
}
